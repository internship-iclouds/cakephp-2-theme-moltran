const webpack = require("webpack")
const path = require("path")
const ExtractTextPlugin = require("extract-text-webpack-plugin")
const WatchTimePlugin = require("webpack-watch-time-plugin")
const glob = require("glob")

const style = new ExtractTextPlugin("./webroot/css/styles.min.css");

module.exports = {
	entry: {
		style: [
            "./Elements/Modal/style.scss"
        ],
        ModalComponent: glob.sync("./Elements/Modal/script.js"),
        DatatableComponent: glob.sync("./Elements/Datatable/script.js"),
	},

	stats: {
		errorDetails: true,
		colors: true,
		modules: true,
		reasons: true
	},

	output: {
		path: path.resolve("webroot"),
		filename: "js/[name].js",
		sourceMapFilename: "js/[name].js.map"
	},

	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader",
				}
			},
			{
                test: /\.scss$/,
                include: /Moltran/,
                use: style.extract(['css-loader', 'sass-loader'])
			}
		]
	},

	plugins: [
		style,

		new WatchTimePlugin({
			noChanges: {
				detect: true,
				report: true
			},
			logLevel: "warn"
		})
	]
}
