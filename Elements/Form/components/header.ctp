<div class="row">
    <div class="col-md-12">
        <ul class="nav nav-tabs tabs" role="tablist" style="width: 100%;">
			<?php foreach ($props['tab'] as $key => $item):?>
				<li class="nav-item tab" style="width: 25%;">
					<a class="nav-link <?= $key == 0 ? 'active' : ''?>" id="<?="tab-$key-tab"?>" data-toggle="tab" href="#<?="tab-$key"?>" role="tab"
						aria-controls="tab-<?=$key?>" aria-selected="true">
						<span class="d-block d-sm-none"><i class="<?= $item['icon']?>"></i></span>
						<span class="d-none d-sm-block"><strong><?= $item['name']?></strong></span>
					</a>
				</li>
			<?php endforeach;?>
        </ul>
    </div>
</div>
