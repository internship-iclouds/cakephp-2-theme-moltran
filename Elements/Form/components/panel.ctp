<div class="col-lg-4 panel panel-default panel-body">
	<div class="card card-default card-fill">
		<div class="card-header row">
			<h4 class="card-title col-md-10"><?= $props['tab'][$tab_active]['panel']['section'][0]['title']?> </h4>
			<button type="button col-md-2" class="btn btn-icon btn-rounded btn-purple btn-sm waves-effect btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar" data-render-modal="1"><i class="fa fa-edit"></i></button>
			<!-- <button class="btn btn-icon btn-rounded btn-danger btn-sm waves-effect btn-default text-danger" data-action="delete" data-id="1" data-toggle="tooltip" data-placement="top" title="" data-original-title="Apagar"><i class="fa fa-trash-o"></i></button> -->
		</div>

		<div class="card-body">
			<?php foreach($props['tab'][$tab_active]['panel']['section'][0]['item'] as $key => $item): ?>
				<div class="about-info-p">
					<strong><?= $item['title'] ?></strong>
					<br>
					<p class="text-muted"><?= $item['value'] ?></p>
				</div>
			<?php endforeach;?>
		</div>
	</div>
</div>

<div class="col-lg-8 ">
	<section class='ml-2 panel panel-default panel-body'>

	</section>
</div>
