<div class="row">
    <div class="col-sm-12">
        <div class="bg-picture text-center" style="background-image:url('assets/images/big/bg.jpg')">
            <div class="bg-picture-overlay"></div>
            <div class="profile-info-name">
                <img src="assets/images/users/avatar-1.jpg" class="thumb-lg rounded-circle img-thumbnail"
                    alt="profile-image">
                <h3 class="text-white">John Deon</h3>
            </div>
        </div>
        <!--/ meta -->
    </div>
</div>
