<div class="">
    <div class="content">
        <div class="container">
			<div class="row">
				<div class="col-sm-12">
					<h4 class="pull-left page-title">titulo</h4>

					<div class="pull-right">

						<div class="d-none d-sm-block col-md-3 col-xl-6 pull-right">
							<div class="pull-right">
								<div class="dropdown">
									<a class="btn btn-primary btn-rounded dropdown-toggle waves-effect waves-light" data-toggle="dropdown"
										aria-haspopup="true" aria-expanded="false">
										Configurar
									</a>
									<ul class="dropdown-menu dropdown-menu-right" role="menu">
										<li><a href="#" class="dropdown-item"><i class="fa fa-edit"></i>  Editar informações</a></li>
										<!-- <li><a href="#" class="dropdown-item"><i class="fa fa-eye"></i>  Ocultar</a></li> -->
										<li><a href="#" class="dropdown-item"><i class="fa fa-trash-o"></i>  Apagar etapa</a></li>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="row ">

				<div class="col-lg-12 panel">
				<?= $this->element('Form/components/header'); ?>

					<div class="tab-content profile-tab-content  panel-default panel-body">
						<?php
							foreach($props['tab'] as $tab_key => $tab){
								$this->viewVars['tab_active'] = $tab_key;
								echo $this->element('Form/components/tab');
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
