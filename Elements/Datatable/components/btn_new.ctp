<?php

if(isset($props['btn']['new'])){
	$props['btn']['new']['url'] = !empty($props['btn']['new']['url']) ? $props['btn']['new']['url'] : 'javascript:;';
	$props['btn']['new']['title'] = !empty($props['btn']['new']['title']) ? $props['btn']['new']['title'] : 'javascript:;';
}

?>

<a
	href="<?= $props['btn']['new']['url'] ?>"
	class="btn btn-md btn-success btn-rounded waves-effect waves-light m-b-5"
	data-btn-add
>
	<i class="fa fa-plus"></i>
	<?= $props['btn']['new']['title'] ?>
</a>
