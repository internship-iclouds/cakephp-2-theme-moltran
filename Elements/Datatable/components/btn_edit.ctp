<button
	type="button"
	class="btn btn-icon btn-sm waves-effect btn-default"
	data-toggle="tooltip"
	data-placement="top"
	title=""
	data-original-title="Editar"
	data-render-modal="<?= $props['id'] ?>"
>
	<i class="fa fa-edit"></i>
</button>
