<a
	href="<?= $props['url'] ?>"
	class="btn btn-info btn-icon btn-sm waves-effect btn-default"
	data-toggle="tooltip"
	data-placement="top"
	title=""
	data-original-title="Ver"
>
	<i class="fa fa-search"></i>
</a>
