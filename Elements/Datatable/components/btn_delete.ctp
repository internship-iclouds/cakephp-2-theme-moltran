<button
	class="btn btn-info btn-icon btn-sm waves-effect btn-danger"
	data-action="delete"
	data-id="<?= $props['id'] ?>"
	data-toggle="tooltip"
	data-placement="top"
	title=""
	data-original-title="Apagar"
>
	<i class="fa fa-trash-o"></i>
</button>
