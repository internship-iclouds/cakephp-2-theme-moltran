export default class Datatable {
	constructor(api = null){
		this.componentDidMount();
		this.watch();
	}

	componentDidMount(){

	}

	watch(){
		$('[data-action="delete"]').click((event) => {
			/**
			 * Get the id of item to delete
			 */
			let $element = $(event.target).parent();
			let id = $($element).attr('data-id');
			if(id == undefined){
				id = $(event.target).attr('data-id');
			}

			/**
			 * open modal to delete idem
			 */
			this.openDeleteModalSwal(id)
		})
	}

	openDeleteModalSwal(id){
		swal({
			title: "Você tem certeza?",
			text: "",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Confirmar",
			closeOnConfirm: false,
			showLoaderOnConfirm: true,
		}, function () {
			$.ajax({
				type: "DELETE",
				url: $('[data-action-delete]').attr('data-action-delete')  + id,
				success: function(){
					swal({
						title: "Sucesso",
						text: "",
						icon: "success",
						type: "success",
						confirmButtonText: "Entendi",
					}, function(){
						location.reload()
					});
				},
				error: function(){
					swal("Ocorreu um erro!", "Você não pode excluir isso porque há informações vinculadas.", "error");
				}
			});
		});
	}
}

new Datatable()
