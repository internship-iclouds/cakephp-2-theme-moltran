<?php require_once 'validations.php'; ?>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title"><?= $props['pageTitle']; ?></h4>

                    <div class="pull-right">
                        <?= $btnNew ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="datatable" class="table table-default table-hover"
                                        data-action-edit="<?= $props['modal']['url']['render'] ?>"
                                        data-action-delete="<?= $props['modal']['url']['delete'] ?>"
                                        data-action-status="<?= $props['modal']['url']['status'] ?>">
                                        <thead>
                                            <tr>
                                                <?php foreach($props['table']['th'] as $key => $prop): ?>
                                                <th style="width:<?=$prop['width'];?>"><?=$prop['title'];?></th>
                                                <?php endforeach; ?>

                                                <?php if(isset($props['btn']['edit'])):?>
                                                <th style="width:1%">Ações</th>
                                                <?php endif;?>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <?php foreach($props['table']['tr'] as $key => $row): ?>
                                            <tr class="gradeX">
                                                <?php foreach($row as $p_key => $prop): ?>
                                                <td>
                                                    <?php if(!empty($prop['url'])):?>
                                                    <a href="<?= $prop['url'] ?>">
                                                        <?php endif;?>
                                                        <span
                                                            class="<?= !empty($prop['label']) ? $prop['label'] : '' ?>"><?= isset($prop['value']) ? $prop['value'] : ''?>
                                                        </span>
                                                        <?php if(!empty($prop['url'])):?>
                                                    </a>
                                                    <?php endif;?>

                                                    <!-- Include btns into td table -->
                                                    <?php

													if(isset($prop['btn'])){


														if(isset($prop['btn']['edit']) && !empty($prop['btn']['edit'])){
															$this->viewVars['props']['url'] = isset($prop['btn']['edit']['url']) ? $prop['btn']['edit']['url'] : '';
															$this->viewVars['props']['id'] = $prop['btn']['edit']['id'];
															echo $this->element('Datatable/components/btn_edit');
														}

														if(isset($prop['btn']['view']) && !empty($prop['btn']['view'])){
															$this->viewVars['props']['url'] = isset($prop['btn']['view']['url']) ? $prop['btn']['view']['url'] : '';
															$this->viewVars['props']['id'] = $prop['btn']['view']['id'];
															echo $this->element('Datatable/components/btn_view');
														}

														if(isset($prop['btn']['delete']) && !empty($prop['btn']['delete'])){
															$this->viewVars['props']['url'] = isset($prop['btn']['delete']['url']) ? $prop['btn']['delete']['url'] : '';
															$this->viewVars['props']['id'] = $prop['btn']['delete']['id'];
															echo $this->element('Datatable/components/btn_delete');
														}
													}
													?>
                                                </td>
                                                <?php endforeach; ?>
                                            </tr>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?= $this->Html->script('DatatableComponent'); ?>
