<?php
	extract($props['modal']);
?>
<!-- Modal -->
<div class="modal fade" id="modalLg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title mt-0" id="custom-width-modalLabel"><?= $props['modal']['title']?></h4>
            </div>

            <div class="modal-body">
                <form id='modal-component' class="form-horizontal" role="form" data-form data-modal-body action="<?= $props['modal']['url']['save'] ?>" method="post">
                    <?= $this->element('Modal/components/body');?>
                </form>
            </div>

            <div class="modal-footer">
				<?php

				if(isset($btn['save']) && $btn['save']){
					echo $this->element('Modal/components/btn_save');
				}

				if(isset($btn['cancel']) && $btn['cancel']){
					echo $this->element('Modal/components/btn_cancel');
				}


				if(isset($btn['ok']) && $btn['ok']){
					echo $this->element('Modal/components/btn_ok');
				}

				if(isset($btn['close']) && $btn['close']){
					echo $this->element('Modal/components/btn_close');
				}

				?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('ModalComponent');?>
