export default class Modal {
	constructor(api = null){
		this.api = $('[data-action-edit]').attr('data-action-edit');

		this.componentDidMount();
		this.watch();
		this.masks()
	}

	componentDidMount() {
		try {
			if(this.api.length == 0){
				throw new Exception('You don\'t set the api url')
			}
		} catch (error) {
			return console.error(error.message)
		}
	}

	watch(){
		$('[data-render-modal]').click((event) => {
			let $element = event.target;
			let id = $($element).attr('data-render-modal');
			if(id == undefined){
				id = $($element).parent().attr('data-render-modal');
			}
			this.render(id)
		})

		$('[data-btn-add]').click(() => this.handleModalAddItem())

		$('[data-btn-save]').click(() => $('[data-form]').submit())
	}

	/**
	 * Get data and update modal with a HTML given by controller
	 */
	render(id = null) {
		$.ajax({
			url: `${this.api}?id=${id}`,
		}).done((body) => {
			$('[data-modal-body]').html(body)
			$('#modalLg').modal()
			this.masks()
		})
	}

	handleModalAddItem(){
		this.cleanForm();
		$('#modalLg').modal()
	}

	cleanForm(){
		$("#modal-component").find('input:text, input:password, input:file, [data-deleteonupdatemodal], select, textarea').val('');
		$("#modal-component").find('input:radio, input:checkbox').removeAttr('checked').removeAttr('selected');
	}

	masks(){
		$('.datemask').inputmask({
			mask: '99/99/9999'
		});

		$('.datemask').datepicker({
			format: 'dd/mm/yyyy',
			language: 'pt-BR'
		});
	}
}

$(document).ready(()=>{
	new Modal()
})
