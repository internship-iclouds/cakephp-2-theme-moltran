<?php if(!empty($props['modal']['descriptions'])): foreach($props['modal']['descriptions'] as $key => $prop): ?>
		<p class="mb-0" style='text-indent:30px'><strong ><?= $prop['title']?></strong>: <?= $prop['value']?></p>
<?php endforeach; endif;?>


<?php if(!empty($props['modal']['inputs'])):  foreach($props['modal']['inputs'] as $key => $prop): ?>
<div class="form-group">
	<?php if($prop['type'] == 'date'): ?>
		<label for="<?= $prop['name']?>" class="col-sm-2 control-label"><?= !empty($prop['label']) ? $prop['label'] . ':' : ''?></label>
		<div class="col-sm-5">
			<div class="input-group">
				<input required="" type="text" class="form-control datemask" placeholder="dd/mm/aaaa" name="<?= $prop['name']?>" aria-required="true" id="<?= $prop['name']?>" value="<?= $prop['value']?>">
				<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
			</div>
		</div>

	<?php elseif($prop['type'] == 'textarea'): ?>
		<label for="<?= $prop['name']?>" class="col-sm-2 control-label"><?= !empty($prop['label']) ? $prop['label'] . ':' : ''?></label>
		<div class="col-sm-10">
			<textarea required="" class="form-control" rows="3" name="<?= $prop['name']?>" aria-required="true"><?= $prop['value']?></textarea>
		</div>

	<?php elseif($prop['type'] == 'select'): ?>
		<label for="<?= $prop['name']?>" class="col-sm-2 control-label"><?= !empty($prop['label']) ? $prop['label'] : ''?>:</label>
		<div class="col-sm-5">
			<select class="form-control" name="<?= $prop['name']?>">
				<option value="">Selecione</option>

				<?php foreach($prop['options'] as $o_key => $option): ?>
					<?php if(!empty($prop['selected']) && $o_key == $prop['selected']): ?>
					<option value="<?=$o_key?>" selected><?=$option?></option>
					<?php else: ?>
					<option value="<?=$o_key?>"><?=$option?></option>
					<?php endif; ?>
				<?php endforeach;?>
			</select>
		</div>
	<?php else: ?>
		<label for="<?= $prop['name']?>" class="col-sm-2 control-label"><?= !empty($prop['label']) ? $prop['label'] . ':' : ''?></label>
		<div class="col-sm-10">
			<input type="<?= $prop['type']?>" name="<?= $prop['name']?>" class="form-control" id="<?= $prop['name']?>"  value="<?= $prop['value']?>" <?= isset($prop['deleteOnUpdateModal']) && $prop['deleteOnUpdateModal'] == true  ? 'data-deleteonupdatemodal=""' : ''?>>
		</div>
	<?php endif; ?>
</div>
<?php endforeach; endif;?>
