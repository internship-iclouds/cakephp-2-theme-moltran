# CakePHP 2 - Moltran Theme Documentation

This documentation provides a tutorial to use the components of Moltran Theme created for CakePHP2

## How to install

1. Go to "app/View/Themed" directory and run the following command on terminal:
```shell
git submodule add https://github.com/gabrieldissotti/theme-moltran.git Moltran
```
> Create the app/View/Themed directory if it don't exists.

2. On beforeRender method of AppController, include the following code:

```php
$this->theme = 'moltran';
```

3. Ready! now you can use the moltran components, thanks for use it, if you like this library, give a star to the project and help giving issues, pull requests or improving documentations.


## How to use components

### Datatable

1.	Include on listing view

```php
<?php

// In view of the list action, use the following schema to mount your page
$this->viewVars['props'] = [
	'pageTitle' => 'Resultados > Parques',

    'table' => [
        'th' => [
            [ 'width' => '87%', 'title'=> 'Título'],
            [ 'width' => '13%', 'title'=> 'Ações'],
        ],
        'tr' => []
	],
];

// Supposing that you given a following $parks array from controller, include it:

foreach ($parks as $key => $park) {
    $this->viewVars['props']['table']['tr'][$key] = [
		// include a new array equal to the following to create new table data
		[
			'value' => "{$park['Park']['title']}", //
			'url_edit' => "/ranking/resultados/parque/{$park['Park']['id']}" // include it only if you need use the input like a link
		],
		[
			'btn' => [
				// use it to edit or view in new page
				'view' => [
					'id' => $park['Park']['id']
				],

				//use it to delete with a modal
				'delete' => [
					'id' => $park['Park']['id']
				]
			]
		],
    ];
}

// Include this element to load the moltran theme and render with the schema that you create int this page.
echo $this->element('Datatable/index');

```

### Modal

1. Include on listing view

```php

// If you already have the "$this->viewVars['props'] = " declaration, include this codes into that already exists, but you don't have, create it now.
$this->viewVars['props'] = [
	'modal' => [
		// Title of the modal
		'title' => 'Editar parque',

		// URLS to use to render dynamically, save or delete the modal
        'url' => [
			'render' => $this->Html->url(['controller' => 'rankings', 'action' => 'renderEditParkModal']),
        	'save' => $this->Html->url(['controller' => 'rankings', 'action' => 'savePark']),
			'delete' =>  $this->Html->url(['controller' => 'rankings', 'action' => 'deletePark']),
		],

		// Buttons to cancel, close, save or to agree(ok), comment to do not show each independent buttons.
		'btn' => [
			'cancel' => true,
			'save' => true,
			'ok' => true,
			// 'close' => true,
		],

		// array of inputs of yout modal, in this point, use only inputs that showing in option "add", (the delete option is feeded by an action of controller)
		'inputs' => [
			[
				'type' => 'text',
				'value' => !empty($park['Park']['title']) ? $park['Park']['title'] : '',
				'label' => 'Nome',
				'name' => 'data[Park][title]',
			],
		],
	],

	// include it to show a btn to create a new item with a modal
	'btn' => [
		'new' => [
			'title' => 'Adicionar parque',
		]
	],


	//use it to edit with a modal
	'edit' => [
		'id' => $park['Park']['id']
	],

```

2. to render the edit modal, create a action according the following example:

```php

public function renderEditParkModal(){
	$parkId = $this->request->query['id'];

	$park = $this->Park->findById($parkId);
	$this->layout = false;

	$props = [
		'modal' => [
			// include all inputs with value of item that have the id informed on option "id" into the element button in view listing.
			'inputs' => [
				[
					'type' => 'hidden',
					'value' => !empty($park['Park']['id']) ? $park['Park']['id'] : '',
					'name' => 'data[Park][id]',
				],
				[
					'type' => 'text',
					'value' => !empty($park['Park']['title']) ? $park['Park']['title'] : '',
					'label' => 'Nome',
					'name' => 'data[Park][title]',
				],
			],
		]
	];

	// set props vars
	$this->set(compact('props'));

	// Render only body of the modal
	$this->render('/Elements/Modal/components/body');
}

```

3. Create routes

```php
// Create routes to actions that you have create, read the session 4 to see examples of actions.
Router::connect('/listagem', array('controller' => 'rankings', 'action' => 'parks'));
Router::connect('/item/:id', array('controller' => 'rankings', 'action' => 'stages'));
Router::connect('item/delete/:id', array('controller' => 'rankings', 'action' => 'deletePark'));

```

4. Others examples of actions
```php
/**
 * Others actions for example:
 */

// Save action example
public function savePark(){
	if(!$this->request->is('post')){
		return false;
	}

	$this->autoRender = false;
	$this->layout = false;
	$data = $this->request->data;

	if($this->Park->save($data)){
		return $this->redirect($this->referer());
	}else{
		return $this->redirect($this->referer());
	}
}

// Delete action example
public function deletePark(){
	$parkId = $this->params['id'];

	$this->layout = false;
	$this->autoRender = false;

	if(!$this->Park->delete($parkId)){
		throw new BadRequestException("Error on deleting park", 400);
	}
}

//Listing action example
public function parks(){
	$this->set('parks', $this->Park->findAll());
}

````
